import cv2
import numpy as np
import pytesseract
from PIL import Image

#Rango del color Azul
azulBajo = np.array([100, 100, 100], np.uint8)
azulAlto = np.array([120, 255, 255], np.uint8)

#Rango de color Rojo
rojoBajo1 = np.array([0, 100, 20], np.uint8)
rojoAlto1 = np.array([10, 255, 255], np.uint8)
rojoBajo2 = np.array([175, 100, 20], np.uint8)
rojoAlto2 = np.array([180, 255, 255], np.uint8)

#Abrir la imagen
imagen = cv2.imread('Page_1.jpg')
imagenHSV = cv2.cvtColor(imagen, cv2.COLOR_BGR2HSV)

#Detectamos colores
maskAzul = cv2.inRange(imagenHSV, azulBajo, azulAlto)

maskRojo1 = cv2.inRange(imagenHSV, rojoBajo1, rojoAlto1)
maskRojo2 = cv2.inRange(imagenHSV, rojoBajo2, rojoAlto2)
maskRojo =  cv2.add(maskRojo1, maskRojo2)

## final mask and masked
cv2.imwrite("target.jpg", maskRojo)

#Crear texto
outfile = 'out_text.txt'
f = open(outfile, 'a')
text = str(((pytesseract.image_to_string(Image.open('target.jpg')))))  
listaPalabras = text.split()
cant9500 = 0
cantptbt = 0
print(listaPalabras)
for i in text:
    if i == '9/500':
        cant9500 = cant9500 + 1
    elif i == 'PTBT':
        cantptbt = cantptbt + 1
print(f'Cantidad de 9/500: {cant9500}')
print(f'Cantidad de PTBT: {cantptbt}')
#text = text.replace('-\n', '')    
#f.write(text)
#f.close()

cv2.imshow('maskRojo', maskRojo)
#cv2.imshow('maskAzul', maskAzul)
cv2.imshow('Imagen', imagen)
cv2.waitKey(0)
cv2.destroyAllWindows()