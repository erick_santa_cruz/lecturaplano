from pdf2image import convert_from_path
import os
import sys
import pytesseract
from PIL import Image

pdfs = 'plano.pdf'
pages = convert_from_path(pdfs,500)

i = 1
for page in pages:
    global image_name
    image_name = "Page_" + str(i) + ".jpg"  
    page.save(image_name, "JPEG")
    i = i+1        


outfile = 'out_text.txt'
  
f = open(outfile, 'a')

text = str(((pytesseract.image_to_string(Image.open(image_name)))))
  
text = text.replace('-\n', '')    

f.write(text)
  
f.close()
